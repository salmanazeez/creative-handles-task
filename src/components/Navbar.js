import React from 'react';
import '../sass/navbar.scss';
import {Navbar,
    Nav, 
    Row,
    Col, 
    NavDropdown,
    } from 'react-bootstrap';

export default class NavBar extends React.Component {
    render() {
        return (
            <div>
                <div className="nav-border">
                    <Navbar collapseOnSelect expand="lg" >
                        <Navbar.Brand href="#home">KLIMAX</Navbar.Brand>
                        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                        <Navbar.Collapse id="responsive-navbar-nav">
                            <Nav className="mr-auto">
                            <Nav.Link href="#home">Home</Nav.Link>
                            <Nav.Link href="#features">Store</Nav.Link>
                            <Nav.Link href="#pricing">About</Nav.Link>
                            <NavDropdown title="Category" id="nav-dropdown">
                                <NavDropdown.Item eventKey="4.1">Air Conditioner</NavDropdown.Item>
                                <NavDropdown.Item eventKey="4.2">Accessories</NavDropdown.Item>
                                <NavDropdown.Item eventKey="4.3">Fans</NavDropdown.Item>
                                <NavDropdown.Divider />
                                <NavDropdown.Item eventKey="4.4">More</NavDropdown.Item>
                            </NavDropdown>
                            </Nav>
                        </Navbar.Collapse>  
                    </Navbar>
                </div>
            </div>
        );
    }
}