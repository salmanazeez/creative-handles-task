import React from 'react';
import NavBar from '../components/Navbar';
import {Container,
        Button,
        Row,
        Col,
        Image, 
        Carousel,
        } from 'react-bootstrap';
import '../sass/home.scss';

export default class Home extends React.Component {
    render() {
        return (
            <div>
                <NavBar></NavBar>
                <div className="container">
                    <div className="heading-section">
                        <h1>Lorem Ipsum is simply dummy</h1>
                        <Button variant="primary">View item</Button>
                    </div>
                </div>
                <Row className="product-description">
                    <Col md={1}></Col>
                    <Col md={1}></Col>
                    <Col xs={12} md={3} className="product-item right-border">
                        <Image src={require('../assets/img/logo1.png')} className="logo-img" fluid />
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since.</p>
                        <Button>More information</Button>
                    </Col>
                    <Col xs={12} md={3} className="right-border">
                        <Image src={require('../assets/img/logo2.png')} className="logo-img mid-img" fluid />
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since.</p>
                        <Button>More information</Button>
                    </Col>
                    <Col xs={12} md={3}>
                        <Image src={require('../assets/img/logo3.png')} className="logo-img" fluid />
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since.</p>
                        <Button>More information</Button>
                    </Col>
                    <Col md={1}></Col>
                </Row>

                <Row className="brand-container">
                    <Col>
                        <h1>New arrivals and brands</h1>
                        <Carousel className="carousal-container">
                            <Carousel.Item>
                                <img
                                className="d-block w-100"
                                src={require('../assets/img/slide1.jpg')}
                                alt="First slide"
                                />
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                className="d-block w-100"
                                src={require('../assets/img/slide2.jpg')}
                                alt="Third slide"
                                />
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                className="d-block w-100"
                                src={require('../assets/img/slide3.jpg')}
                                alt="Third slide"
                                />
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                className="d-block w-100"
                                src={require('../assets/img/slide4.jpg')}
                                alt="Third slide"
                                />
                            </Carousel.Item>
                        </Carousel>
                    </Col>
                </Row>
                
                <div className="description-container">
                    <Row>
                        <Col xs={12} md={6}>
                            <div className="text-container">
                                <h1>Lorem Ipsum</h1>
                                <p>It is a long established fact that a reader will be <a href="#">distracted</a> by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like <a href="#">readable English</a>. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many <a href="#">web sites</a> still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by <a href="#">injected humour</a>, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                            </div>
                        </Col>
                        <div className="icon-container">
                            <Row>
                                <Col xs={12} md={4} className="icons">
                                    <img className="d-block" src={require('../assets/img/cart.png')} />
                                    <p><strong>Shop with us</strong></p>
                                </Col>
                                <Col xs={12} md={4} className="icons">
                                    <img className="d-block" src={require('../assets/img/ac.png')} />
                                    <p><strong>We repair A/C</strong></p>
                                </Col>
                                <Col xs={12} md={4} className="icons">
                                    <img className="d-block" src={require('../assets/img/thermo.png')} />
                                    <p><strong>We service car</strong></p>
                                </Col>
                                <Col xs={12} md={4} className="icons">
                                    <img className="d-block" src={require('../assets/img/map.png')} />
                                    <p><strong>We are located</strong></p>
                                </Col>
                            </Row>
                        </div>
                    </Row>
                </div> 

                <div className="btn-container">
                    <Button>More references</Button>
                </div>

                <div className="details-container">
                    <Row>
                        <Col xs={12} md={6}>
                            <div className="text-container">
                                <h1>Lorem Ipsum</h1>
                                <p>It is a long established fact that a reader will be <a href="#">distracted</a> by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like <a href="#">readable English</a>. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many <a href="#">web sites</a> still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by <a href="#">injected humour</a>, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                            </div>
                        </Col>
                        <Col xs={12} md={6} className="align-container">
                            <Image src={require('../assets/img/fan.jpg')} fluid />
                        </Col>
                    </Row>
                </div>
                <div className="contact-container">
                    <Row>
                        <Col xs={12} md={6} className="border-right">
                            <Image src={require('../assets/img/phone.png')} fluid />
                            <h3>+420 123 456 789</h3>
                        </Col>
                        <Col xs={12} md={6}>
                            <Image src={require('../assets/img/mail.png')} fluid />
                            <h3>info@klimax.cz</h3>
                        </Col>
                    </Row>
                </div>
                <div className="footer">
                    <Row>
                        <Col xs={12} md={6} className="footer-contents">
                            <h1>KLIMAX</h1>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </Col>
                        <Col xs={12} md={1} className="links footer-contents">
                            <h5>Links</h5>
                            <a href="#">Home</a>
                            <a href="#">About</a>
                            <a href="#">Services</a>
                            <a href="#">Contact</a>
                        </Col>
                        <Col xs={12} md={3} className="address footer-contents">
                            <h5>Reach us</h5>
                            <p>KLIMAX tarace co, Building road, 417 23, Lorem Ipsum.</p>
                            <p>Telephone: +458 123 456 789</p>
                            <p>Email: info@klimax.com</p>
                        </Col>
                        <Col xs={12} md={2} className="footer-contents">
                            <h5>Social media</h5>
                        </Col>
                    </Row>
                </div>
                <div className="copyrights">
                    <Row>
                        <Col xs={12} md={6}>
                            <p>©KLIMAX 2020 company private limited</p>
                        </Col>
                        <Col xs={12} md={6}>©Copyrights All rights reserved</Col>
                    </Row>
                </div>
            </div>
        );
    }
}